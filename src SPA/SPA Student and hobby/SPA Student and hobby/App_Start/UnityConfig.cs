﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Microsoft.Practices.Unity;
using SPA_Student_and_hobby.Controllers;
using SPA_Student_and_hobby.Infrastructure;
using SPA_Student_and_hobby.Models;

namespace SPA_Student_and_hobby.App_Start
{
     public static class UnityConfig
     {
          public static void Register(HttpConfiguration config)
          {
               var container = new UnityContainer();
               container.RegisterType<IGroupRepository, GroupRepository>(new HierarchicalLifetimeManager());
               container.RegisterType<IStudentRepository, StudentRepository>(new HierarchicalLifetimeManager());
               container.RegisterType<IHobbyRepository, HobbyRepository>(new HierarchicalLifetimeManager());
               container.RegisterType<HomeController>();
               config.DependencyResolver = new UnityResolver(container);
               //// Register the Controllers that should be injectable
               //unity.RegisterType<GroupController>();
               //unity.RegisterType<HobbyController>();
               //unity.RegisterType<StudentController>();
               //unity.RegisterType<HomeController>();

               //// Register instances to be used when resolving constructor parameter dependencies
               //unity.RegisterInstance<IGroupRepository>(new GroupRepository());
               //unity.RegisterInstance<IStudentRepository>(new StudentRepository());
               //unity.RegisterInstance<IHobbyRepository>(new HobbyRepository());
               //DependencyResolver.SetResolver(new UnityDependencyResolver(unity));
               //// Finally, override the default dependency resolver with Unity
               //GlobalConfiguration.Configuration.DependencyResolver = new IoCContainer(unity);
          }
     }
}