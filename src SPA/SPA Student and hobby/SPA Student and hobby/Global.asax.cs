﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using SPA_Student_and_hobby.App_Start;
using SPA_Student_and_hobby.Infrastructure;
using SPA_Student_and_hobby.Controllers;
using SPA_Student_and_hobby.Models;
using Microsoft.Practices.Unity;
using System.Web.Http.Dependencies;

namespace SPA_Student_and_hobby
{
     // Примечание: Инструкции по включению классического режима IIS6 или IIS7 
     // см. по ссылке http://go.microsoft.com/?LinkId=9394801

     public class WebApiApplication : System.Web.HttpApplication
     {
          protected void Application_Start()
          {
               AreaRegistration.RegisterAllAreas();
               UnityConfig.Register(GlobalConfiguration.Configuration);
               WebApiConfig.Register(GlobalConfiguration.Configuration);
               FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
               RouteConfig.RegisterRoutes(RouteTable.Routes);
               BundleConfig.RegisterBundles(BundleTable.Bundles);
               GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
          }
     }
}