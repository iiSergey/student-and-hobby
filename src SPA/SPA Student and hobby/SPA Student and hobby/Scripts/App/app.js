﻿var app = angular.module('app', ['ngRoute','ngResource']);
app.config(['$routeProvider',
  function($routeProvider) {
      $routeProvider.
        when('/', {
            templateUrl: '/ViewsContent/Home/Index.html',
          //  controller: 'HobbyCtrl'
        }).
        when('/hobby', {
            templateUrl: '/ViewsContent/Hobby/Index.html',
        }).
        otherwise({
            redirectTo: '/'
        });
  }]);

