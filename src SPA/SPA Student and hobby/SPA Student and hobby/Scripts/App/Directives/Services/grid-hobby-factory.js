﻿var url = 'api/hobby/';

app.factory('hobbyFactory', function ($http) {
    return {
        getHobbies: function () {
            return $http.get(url);
        },
        addHobby: function (hobby) {
            return $http.post(url, hobby);
        },
        deleteHobby: function (hobby) {
            return $http.delete(url + hobby.HobbyId);
        },
        updateHobby: function (hobby) {
            return $http.put(url + hobby.HobbyId, hobby);
        }
    };
});