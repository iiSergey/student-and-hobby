﻿
app.controller('HobbyCtrl', function ($scope, hobbyFactory, notificationFactory) {
    $scope.hobbies = [];
    $scope.addMode = false;
    $scope.orderBy = { field: 'Name', asc: true };
    $scope.loading = true;

    $scope.toggleAddMode = function () {
        $scope.addMode = !$scope.addMode;
    };

    $scope.toggleEditMode = function (hobby) {
        hobby.editMode = !hobby.editMode;
    };

    $scope.toggleDetaliesMode = function (hobby) {
        hobby.detaliesMode = !hobby.detaliesMode;
    };

    var successCallback = function (e, cb) {
        notificationFactory.success();
        $scope.getData(cb);
    };

    var successPostCallback = function (e) {
        successCallback(e, function () {
            $scope.toggleAddMode();
            $scope.hobby = {};
        });
    };

    var errorCallback = function (e) {
        notificationFactory.error(e.ExceptionMessage);
    };

    $scope.getData = function (cb) {
        hobbyFactory.getHobbies().success(function (data) {
            $scope.hobbies = data;
            if (typeof (cb) === "function") cb();
        }).error(function (e) {
            notificationFactory.error(e.Message);
        });
    };

    $scope.addHobby = function () {
        hobby.Students = null;
        hobbyFactory.addHobby($scope.hobby).success(successPostCallback).error(errorCallback);
    };

    $scope.deleteHobby = function (hobby) {
        hobby.Students = null;
        hobbyFactory.deleteHobby(hobby).success(successCallback).error(errorCallback);
    };

    $scope.updateHobby = function (hobby) {
        hobby.Students = null;
        hobbyFactory.updateHobby(hobby).success(successCallback).error(errorCallback);
    };

    $scope.setOrderBy = function (field) {
        var asc = $scope.orderBy.field === field ? !$scope.orderBy.asc : true;
        $scope.orderBy = { field: field, asc: asc };
    }

    $scope.getData(function () { $scope.loading = false; });
});