﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SPA_Student_and_hobby.Models;

namespace SPA_Student_and_hobby.Controllers
{
     public class HobbyController : ApiController
     {
          private readonly IHobbyRepository hobbyRepository;

          public HobbyController(IHobbyRepository hobbyRepository)
          {
               this.hobbyRepository = hobbyRepository;
          }

          // GET api/hobby
          public IEnumerable<Hobby> Get()
          {
               return hobbyRepository.AllIncluding(hobby => hobby.Students).AsEnumerable();
          }

          // GET api/hobby/5
          public Hobby Get(int id)
          {
               Hobby hobby = hobbyRepository.Find(id);
               if (hobby == null)
               {
                    throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
               }
               return hobby;
          }

          // POST api/hobby
          public HttpResponseMessage Post(Hobby hobby)
          {
               if (ModelState.IsValid)
               {
                    hobbyRepository.InsertOrUpdate(hobby);
                    hobbyRepository.Save();

                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, hobby);
                    response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = hobby.HobbyId }));
                    return response;
               }
               else
               {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
               }
          }

          // PUT api/hobby/5
          public HttpResponseMessage Put(int id, Hobby hobby)
          {
               if (!ModelState.IsValid)
               {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
               }
               if (id != hobby.HobbyId)
               {
                    return Request.CreateResponse(HttpStatusCode.BadRequest);
               }
               hobbyRepository.InsertOrUpdate(hobby);
               try
               {
                    hobbyRepository.Save();
               }
               catch (DbUpdateConcurrencyException ex)
               {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
               }
               return Request.CreateResponse(HttpStatusCode.OK);
          }

          // DELETE api/hobby/5
          public HttpResponseMessage Delete(int id)
          {
               Hobby hobby = hobbyRepository.Find(id);
               if (hobby == null)
               {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
               }
               hobbyRepository.Delete(id);

               try
               {
                    hobbyRepository.Save();
               }
               catch (DbUpdateConcurrencyException ex)
               {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
               }

               return Request.CreateResponse(HttpStatusCode.OK, hobby);
          }
     }
}