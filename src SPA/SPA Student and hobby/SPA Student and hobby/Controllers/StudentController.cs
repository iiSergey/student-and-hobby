﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SPA_Student_and_hobby.Models;

namespace SPA_Student_and_hobby.Controllers
{
     public class StudentController : ApiController
     {
          private readonly IStudentRepository studentRepository;

          public StudentController(IStudentRepository studentRepository)
          {
               this.studentRepository = studentRepository;
          }

          // GET api/student
          public IEnumerable<Student> Get()
          {
               return studentRepository.AllIncluding(student => student.Group, student => student.Hobbies);
          }

          // GET api/student/5
          public Student Get(int id)
          {
               Student student = studentRepository.Find(id);
               if (student == null)
               {
                    throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
               }
               return student;
          }

          // POST api/student
          public HttpResponseMessage Post(Student student)
          {
               if (ModelState.IsValid)
               {
                    studentRepository.InsertOrUpdate(student);
                    studentRepository.Save();

                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, student);
                    response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = student.StudentId }));
                    return response;
               }
               else
               {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
               }
          }

          // PUT api/student/5
          public HttpResponseMessage Put(int id, Student student)
          {
               if (!ModelState.IsValid)
               {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
               }
               if (id != student.StudentId)
               {
                    return Request.CreateResponse(HttpStatusCode.BadRequest);
               }
               studentRepository.InsertOrUpdate(student);
               try
               {
                    studentRepository.Save();
               }
               catch (DbUpdateConcurrencyException ex)
               {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
               }
               return Request.CreateResponse(HttpStatusCode.OK);
          }

          // DELETE api/student/5
          public HttpResponseMessage Delete(int id)
          {
               Student student = studentRepository.Find(id);
               if (student == null)
               {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
               }
               studentRepository.Delete(id);

               try
               {
                    studentRepository.Save();
               }
               catch (DbUpdateConcurrencyException ex)
               {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
               }

               return Request.CreateResponse(HttpStatusCode.OK, student);
          }
     }
}