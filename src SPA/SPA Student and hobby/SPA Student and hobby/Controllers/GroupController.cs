﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SPA_Student_and_hobby.Models;

namespace SPA_Student_and_hobby.Controllers
{
     public class GroupController : ApiController
     {
          private readonly IGroupRepository groupRepository;

          public GroupController(IGroupRepository groupRepository)
          {
               this.groupRepository = groupRepository;
          }

          // GET api/group
          public IEnumerable<Group> Get()
          {
               return groupRepository.AllIncluding(group => group.Students);
          }

          // GET api/group/5
          public Group Get(int id)
          {
               Group group = groupRepository.Find(id);
               if (group == null)
               {
                    throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
               }
               return group;
          }

          // POST api/group
          public HttpResponseMessage Post(Group group)
          {
               if (ModelState.IsValid)
               {
                    groupRepository.InsertOrUpdate(group);
                    groupRepository.Save();

                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, group);
                    response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = group.GroupId }));
                    return response;
               }
               else
               {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
               }
          }

          // PUT api/group/5
          public HttpResponseMessage Put(int id, Group group)
          {
               if (!ModelState.IsValid)
               {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
               }
               if (id != group.GroupId)
               {
                    return Request.CreateResponse(HttpStatusCode.BadRequest);
               }
               groupRepository.InsertOrUpdate(group);
               try
               {
                    groupRepository.Save();
               }
               catch (DbUpdateConcurrencyException ex)
               {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
               }
               return Request.CreateResponse(HttpStatusCode.OK);
          }

          // DELETE api/group/5
          public HttpResponseMessage Delete(int id)
          {
               Group group = groupRepository.Find(id);
               if (group == null)
               {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
               }
               groupRepository.Delete(id);

               try
               {
                    groupRepository.Save();
               }
               catch (DbUpdateConcurrencyException ex)
               {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
               }

               return Request.CreateResponse(HttpStatusCode.OK, group);
          }
     }
}