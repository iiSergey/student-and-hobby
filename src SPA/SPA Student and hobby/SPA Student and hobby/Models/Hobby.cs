﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SPA_Student_and_hobby.Models
{
     [KnownType(typeof(Student))]
     [KnownType(typeof(Gender))]
     [KnownType(typeof(Group))]
     public class Hobby
     {
          public int HobbyId { get; set; }

          [Required(ErrorMessage = "Введіть назву хобі", AllowEmptyStrings = false)]
          [MaxLength(100, ErrorMessage = "Назва надто довга")]
          public string Name { get; set; }

          public virtual ICollection<Student> Students { get; set; }
     }
}