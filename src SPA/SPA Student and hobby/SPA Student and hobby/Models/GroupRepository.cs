using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace SPA_Student_and_hobby.Models
{ 
    public class GroupRepository : IGroupRepository
    {
        StudentAndHobbyContext context = new StudentAndHobbyContext();

        public IQueryable<Group> All
        {
            get { return context.Group; }
        }

        public IQueryable<Group> AllIncluding(params Expression<Func<Group, object>>[] includeProperties)
        {
            IQueryable<Group> query = context.Group;
            foreach (var includeProperty in includeProperties) {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public Group Find(int id)
        {
            return context.Group.Find(id);
        }

        public void InsertOrUpdate(Group group)
        {
            if (group.GroupId == default(int)) {
                // New entity
                context.Group.Add(group);
            } else {
                // Existing entity
                context.Entry(group).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var group = context.Group.Find(id);
            context.Group.Remove(group);
        }

        public void Save()
        {
            context.SaveChanges();
        }

        public void Dispose() 
        {
            context.Dispose();
        }
    }

    public interface IGroupRepository : IDisposable
    {
        IQueryable<Group> All { get; }
        IQueryable<Group> AllIncluding(params Expression<Func<Group, object>>[] includeProperties);
        Group Find(int id);
        void InsertOrUpdate(Group group);
        void Delete(int id);
        void Save();
    }
}