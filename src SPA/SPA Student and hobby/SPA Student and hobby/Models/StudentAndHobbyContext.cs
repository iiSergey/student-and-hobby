﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace SPA_Student_and_hobby.Models
{
     public class StudentAndHobbyContext : DbContext
     {
          public StudentAndHobbyContext()
          {
               this.Configuration.LazyLoadingEnabled = false;
               this.Configuration.ProxyCreationEnabled = false;
          }
          protected override void OnModelCreating(DbModelBuilder modelBuilder)
          {            
               modelBuilder.Entity<Student>()
                .HasMany(p => p.Hobbies)
                .WithMany(t => t.Students)
                .Map(mc =>
                {
                     mc.ToTable("StudentJoinHobby");
                     mc.MapLeftKey("StudentId");
                     mc.MapRightKey("HobbyId");
                });
          }
          public DbSet<SPA_Student_and_hobby.Models.Group> Group { get; set; }

          public DbSet<SPA_Student_and_hobby.Models.Student> Student { get; set; }

          public DbSet<SPA_Student_and_hobby.Models.Hobby> Hobby { get; set; }
     }
}