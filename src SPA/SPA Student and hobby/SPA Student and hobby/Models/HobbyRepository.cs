using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Query;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace SPA_Student_and_hobby.Models
{ 
    public class HobbyRepository : IHobbyRepository
    {
         StudentAndHobbyContext context = new StudentAndHobbyContext() ;
         public HobbyRepository()
         {
            //  context.Configuration.ProxyCreationEnabled = false;
         }
        public IQueryable<Hobby> All
        {
            get { return context.Hobby; }
        }

        public IQueryable<Hobby> AllIncluding(params Expression<Func<Hobby, object>>[] includeProperties)
        {
             IQueryable<Hobby> query = context.Hobby;
             foreach (var includeProperty in includeProperties)
             {
                  query = query.Include(includeProperty);
             }
            return query;
        }

        public Hobby Find(int id)
        {
            return context.Hobby.Find(id);
        }

        public void InsertOrUpdate(Hobby hobby)
        {
            if (hobby.HobbyId == default(int)) {
                // New entity
                context.Hobby.Add(hobby);
            } else {
                // Existing entity
                context.Entry(hobby).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var hobby = context.Hobby.Find(id);
            context.Hobby.Remove(hobby);
        }

        public void Save()
        {
            context.SaveChanges();
        }

        public void Dispose() 
        {
            context.Dispose();
        }
    }

    public interface IHobbyRepository : IDisposable
    {
        IQueryable<Hobby> All { get; }
        IQueryable<Hobby> AllIncluding(params Expression<Func<Hobby, object>>[] includeProperties);
        Hobby Find(int id);
        void InsertOrUpdate(Hobby hobby);
        void Delete(int id);
        void Save();
    }
}