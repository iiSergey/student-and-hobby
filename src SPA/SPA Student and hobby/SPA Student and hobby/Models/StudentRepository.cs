using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace SPA_Student_and_hobby.Models
{ 
    public class StudentRepository : IStudentRepository
    {
        StudentAndHobbyContext context = new StudentAndHobbyContext();

        public IQueryable<Student> All
        {
            get { return context.Student; }
        }
        public IQueryable<Hobby> AllHobby
        {
             get { return context.Hobby; }
        }

        public IQueryable<Student> AllIncluding(params Expression<Func<Student, object>>[] includeProperties)
        {
            IQueryable<Student> query = context.Student;
            foreach (var includeProperty in includeProperties) {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public Student Find(int id)
        {
            return context.Student.Find(id);
        }

        public void InsertOrUpdate(Student student)
        {
            if (student.StudentId == default(int)) {
                // New entity
                context.Student.Add(student);
            } else {
                // Existing entity
                context.Entry(student).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var student = context.Student.Find(id);
            context.Student.Remove(student);
        }

        public void Save()
        {
            context.SaveChanges();
        }

        public void Dispose() 
        {
            context.Dispose();
        }
    }

    public interface IStudentRepository : IDisposable
    {
        IQueryable<Student> All { get; }
        IQueryable<Hobby> AllHobby { get; }
        IQueryable<Student> AllIncluding(params Expression<Func<Student, object>>[] includeProperties);
        Student Find(int id);
        void InsertOrUpdate(Student student);
        void Delete(int id);
        void Save();
    }
}