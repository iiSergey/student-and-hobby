﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SPA_Student_and_hobby.Models
{
     public enum Gender { Male = 0, Female = 1 }
     public class Student
     {
          public int StudentId { get; set; }

          [Display(Name = "Група")]
          public int GroupId { get; set; }

          [Display(Name = "Ім'я студента")]
          [Required(ErrorMessage = "Введіть ім'я студента", AllowEmptyStrings = false)]
          [MaxLength(50, ErrorMessage = "Ім'я надто довге")]
          public string Name { get; set; }

          [Display(Name = "Прізвище студента")]
          [Required(ErrorMessage = "Введіть прізвище", AllowEmptyStrings = false)]
          [MaxLength(50, ErrorMessage = "Прізвище надто довге")]
          public string Surname { get; set; }

          [Display(Name = "Ім'я по-батькові студента")]
          [Required(ErrorMessage = "Введіть ім'я по-батькові", AllowEmptyStrings = false)]
          [MaxLength(50, ErrorMessage = "Ім'я по-батькові надто довге")]
          public string Patronymic { get; set; }

          [Display(Name = "Дата рождения")]
          [DataType(DataType.Date, ErrorMessage = "Введіть дату народження")]
          [DisplayFormat(DataFormatString = "{0:dd'/'MM'/'yyyy}", ApplyFormatInEditMode = true)]
          [Required(ErrorMessage = "Введіть дату народження", AllowEmptyStrings = false)]
          public DateTime Birthdey { get; set; }

          [Display(Name = "Стать студента")]
          [Required(ErrorMessage = "Введіть стать студента", AllowEmptyStrings = false)]
          public Gender Gender { get; set; }

          public virtual Group Group { get; set; }

          public virtual ICollection<Hobby> Hobbies { get; set; }
     }
}