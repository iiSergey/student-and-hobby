﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace SPA_Student_and_hobby.Models
{
     public class Group
     {
          public int GroupId { get; set; }

          [Display(Name = "Назва групи")]
          [Required(ErrorMessage="Введіть назву групи",AllowEmptyStrings=false)]
          [MaxLength(50,ErrorMessage="Назва надто довга")]
          public string Name { get; set; }

          [Display(Name = "Курс навчання")]
          [Required(ErrorMessage = "Введіть курс навчання (1-6)", AllowEmptyStrings = false)]
          [Range(1, 6, ErrorMessage = "Введіть курс навчання (1-6)")]
          public int Year { get; set; }

          [Display(Name = "Спеціальність")]
          [Required(ErrorMessage = "Введіть назву спеціальності", AllowEmptyStrings = false)]
          [MaxLength(50, ErrorMessage = "Назва надто довга")]
          public string Specialty { get; set; }

          [Display(Name = "Кафедра")]
          [Required(ErrorMessage = "Введіть назву кафедри", AllowEmptyStrings = false)]
          [MaxLength(50, ErrorMessage = "Назва надто довга")]
          public string Department { get; set; }

          [Display(Name = "Факультет")]
          [Required(ErrorMessage = "Введіть назву факультету", AllowEmptyStrings = false)]
          [MaxLength(50, ErrorMessage = "Назва надто довга")]
          public string Faculty { get; set; }

          public virtual ICollection<Student> Students { get; set; }
     }
}