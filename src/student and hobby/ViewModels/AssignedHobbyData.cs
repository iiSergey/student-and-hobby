﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace student_and_hobby.ViewModels
{
     public class AssignedHobbyData
     {
          public int HobbyID { get; set; }
          public string Title { get; set; }
          public bool Assigned { get; set; } 
     }
}