﻿using System.Web;
using System.Web.Mvc;

namespace student_and_hobby
{
     public class FilterConfig
     {
          public static void RegisterGlobalFilters(GlobalFilterCollection filters)
          {
               filters.Add(new HandleErrorAttribute());
          }
     }
}