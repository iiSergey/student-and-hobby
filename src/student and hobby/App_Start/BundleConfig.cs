﻿using System.Web;
using System.Web.Optimization;

namespace student_and_hobby
{
     public class BundleConfig
     {
          // Дополнительные сведения о Bundling см. по адресу http://go.microsoft.com/fwlink/?LinkId=254725
          public static void RegisterBundles(BundleCollection bundles)
          {
               RegisterStyleBundles(bundles);
               RegisterJavascriptBundles(bundles);
               RegisterBootstrapDatepicker(bundles);
          }

          private static void RegisterStyleBundles(BundleCollection bundles)
          {
               bundles.Add(new StyleBundle("~/css")
                               .Include("~/Content/bootstrap.css")
                               .Include("~/Content/site.css"));
          }

          private static void RegisterJavascriptBundles(BundleCollection bundles)
          {
               bundles.Add(new ScriptBundle("~/js")
                               .Include("~/Scripts/jquery-{version}.js")
                               .Include("~/Scripts/jquery-ui-{version}.js")
                               .Include("~/Scripts/bootstrap.js"));
          }

          private static void RegisterBootstrapDatepicker(BundleCollection bundles)
          {
               bundles.Add(new StyleBundle("~/datepicker/css")
                               .Include("~/Content/datepicker/datepicker.css"));

               bundles.Add(new ScriptBundle("~/datepicker/js")
                               .Include("~/Scripts/datepicker/bootstrap-datepicker.js")
                               .Include("~/Scripts/datepicker/bootstrap-datepicker.ua.js")
                               .Include("~/Scripts/datepicker/add-bootstrap-datepicker.js"));
          }
     }
}