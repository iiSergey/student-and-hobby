using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using student_and_hobby.Models;
using student_and_hobby.Infrastructure;

namespace student_and_hobby.Controllers
{   
    public class GroupController : Controller
    {
         private readonly IGroupRepository groupRepository;


        public GroupController(IGroupRepository groupRepository)
         {
              this.groupRepository = groupRepository;
        }

        //
        // GET: /Group/

        public ViewResult Index()
        {
             return View(groupRepository.AllIncluding(group => group.Students));
        }

        //
        // GET: /Group/Details/5

        public ViewResult Details(int id)
        {
            return View(groupRepository.Find(id));
        }

        //
        // GET: /Group/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /Group/Create

        [HttpPost]
        public ActionResult Create(Group group)
        {
            if (ModelState.IsValid) {
                groupRepository.InsertOrUpdate(group);
                groupRepository.Save();
                return RedirectToAction("Index");
            } else {
				return View();
			}
        }
        
        //
        // GET: /Group/Edit/5
 
        public ActionResult Edit(int id)
        {
             return View(groupRepository.Find(id));
        }

        //
        // POST: /Group/Edit/5

        [HttpPost]
        public ActionResult Edit(Group group)
        {
            if (ModelState.IsValid) {
                groupRepository.InsertOrUpdate(group);
                groupRepository.Save();
                return RedirectToAction("Index");
            } else {
				return View();
			}
        }

        //
        // GET: /Group/Delete/5
 
        public ActionResult Delete(int id)
        {
            return View(groupRepository.Find(id));
        }

        //
        // POST: /Group/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            groupRepository.Delete(id);
            groupRepository.Save();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) {
                groupRepository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

