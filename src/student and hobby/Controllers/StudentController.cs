using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using student_and_hobby.Models;
using student_and_hobby.ViewModels;

namespace student_and_hobby.Controllers
{   
    public class StudentController : Controller
    {
		private readonly IGroupRepository groupRepository;
          private readonly IStudentRepository studentRepository;

          public StudentController(IGroupRepository groupRepository, IStudentRepository studentRepository)
        {
			this.groupRepository = groupRepository;
			this.studentRepository = studentRepository;
        }

        //
        // GET: /Student/

        public ViewResult Index()
        {
            return View(studentRepository.AllIncluding(student => student.Group, student => student.Hobbies));
        }

        //
        // GET: /Student/Details/5

        public ViewResult Details(int id)
        {
            return View(studentRepository.Find(id));
        }

        //
        // GET: /Student/Create

        public ActionResult Create()
        {
             PopulateAssignedHobbyData();
            ViewBag.PossibleGroup = groupRepository.All;
            return View();
        }

        private void PopulateAssignedHobbyData()
        {
             var allHobby = studentRepository.AllHobby;
             var viewModel = new List<AssignedHobbyData>();
             foreach (var hobby in allHobby)
             {
                  viewModel.Add(new AssignedHobbyData
                  {
                       HobbyID = hobby.HobbyId,
                       Title = hobby.Name,
                       Assigned = false
                  });
             }
             ViewBag.Hobby = viewModel;
        }
        //
        // POST: /Student/Create

        [HttpPost]
        public ActionResult Create(Student student, string[] selectedHobby)
        {
             if (ModelState.IsValid)
             {
                  UpdateInstructorCourses(student, selectedHobby);
                  studentRepository.InsertOrUpdate(student);
                  studentRepository.Save();
                  return RedirectToAction("Index");
             }
             else
             {
                  ViewBag.PossibleGroup = groupRepository.All;
                  return View();
             }

            if (ModelState.IsValid) {
                studentRepository.InsertOrUpdate(student);
                studentRepository.Save();
                return RedirectToAction("Index");
            } else {
				ViewBag.PossibleGroup = groupRepository.All;
				return View();
			}
        }
        
        //
        // GET: /Student/Edit/5
 
        public ActionResult Edit(int id)
        {
             Student student = studentRepository.Find(id);
             PopulateAssignedHobbyData(student);
			ViewBag.PossibleGroup = groupRepository.All;
               return View(student);
        }

        private void PopulateAssignedHobbyData(Student student)
        {
             var allHobby = studentRepository.AllHobby;
             var instructorHobby =student.Hobbies==null?new HashSet<int>(): new HashSet<int>(student.Hobbies.Select(c => c.HobbyId));
             var viewModel = new List<AssignedHobbyData>();
             foreach (var hobby in allHobby)
             {
                  viewModel.Add(new AssignedHobbyData
                  {
                       HobbyID = hobby.HobbyId,
                       Title = hobby.Name,
                       Assigned = instructorHobby.Contains(hobby.HobbyId)
                  });
             }
             ViewBag.Hobby = viewModel;
        }

        //
        // POST: /Student/Edit/5

        [HttpPost]
        public ActionResult Edit(Student student, string[] selectedHobby)
        {
                UpdateInstructorCourses(student, selectedHobby);
            if (ModelState.IsValid) 
            {                 
                studentRepository.InsertOrUpdate(student);
                studentRepository.Save();
                return RedirectToAction("Index");
            } 
            else 
            {
                 PopulateAssignedHobbyData(student);
                 ViewBag.PossibleGroup = groupRepository.All;
                 return View(student);
			}
        }
        private void UpdateInstructorCourses(Student student, string[] selectedCourses)
        {
             if (selectedCourses == null)
             {
                  student.Hobbies = new List<Hobby>();
                  return;
             }

             var selectedHobbiesHS = new HashSet<string>(selectedCourses);
             if (student.Hobbies == null)
             {
                  student.Hobbies = new List<Hobby>();
                  foreach (var hobby in studentRepository.AllHobby)
                  {
                       if (selectedHobbiesHS.Contains(hobby.HobbyId.ToString()))
                       {
                                 student.Hobbies.Add(hobby);
                            hobby.Students.Add(student);
                       }
                  }
             }
             else
             {
                  var studentHobbies = new HashSet<int>
                      (student.Hobbies.Select(c => c.HobbyId));
                  foreach (var hobby in studentRepository.AllHobby)
                  {
                       if (selectedHobbiesHS.Contains(hobby.HobbyId.ToString()))
                       {
                            if (!studentHobbies.Contains(hobby.HobbyId))
                            {
                                 student.Hobbies.Add(hobby);
                                 hobby.Students.Add(student);
                            }
                       }
                       else
                       {
                            if (studentHobbies.Contains(hobby.HobbyId))
                            {
                                 student.Hobbies.Remove(hobby);
                                 hobby.Students.Remove(student);
                            }
                       }
                  }
             }
        }
        //
        // GET: /Student/Delete/5
 
        public ActionResult Delete(int id)
        {
            return View(studentRepository.Find(id));
        }

        //
        // POST: /Student/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            studentRepository.Delete(id);
            studentRepository.Save();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) {
                groupRepository.Dispose();
                studentRepository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

