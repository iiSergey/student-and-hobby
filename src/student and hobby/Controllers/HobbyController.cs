using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using student_and_hobby.Models;

namespace student_and_hobby.Controllers
{   
    public class HobbyController : Controller
    {
		private readonly IHobbyRepository hobbyRepository;


        public HobbyController(IHobbyRepository hobbyRepository)
        {
			this.hobbyRepository = hobbyRepository;
        }

        //
        // GET: /Hobby/

        public ViewResult Index()
        {
            return View(hobbyRepository.AllIncluding(hobby => hobby.Students));
        }

        //
        // GET: /Hobby/Details/5

        public ViewResult Details(int id)
        {
            return View(hobbyRepository.Find(id));
        }

        //
        // GET: /Hobby/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /Hobby/Create

        [HttpPost]
        public ActionResult Create(Hobby hobby)
        {
            if (ModelState.IsValid) {
                hobbyRepository.InsertOrUpdate(hobby);
                hobbyRepository.Save();
                return RedirectToAction("Index");
            } else {
				return View();
			}
        }
        
        //
        // GET: /Hobby/Edit/5
 
        public ActionResult Edit(int id)
        {
             return View(hobbyRepository.Find(id));
        }

        //
        // POST: /Hobby/Edit/5

        [HttpPost]
        public ActionResult Edit(Hobby hobby)
        {
            if (ModelState.IsValid) {
                hobbyRepository.InsertOrUpdate(hobby);
                hobbyRepository.Save();
                return RedirectToAction("Index");
            } else {
				return View();
			}
        }

        //
        // GET: /Hobby/Delete/5
 
        public ActionResult Delete(int id)
        {
            return View(hobbyRepository.Find(id));
        }

        //
        // POST: /Hobby/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            hobbyRepository.Delete(id);
            hobbyRepository.Save();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) {
                hobbyRepository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

