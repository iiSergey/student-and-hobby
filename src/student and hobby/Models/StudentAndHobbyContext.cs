﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace student_and_hobby.Models
{
     public class StudentAndHobbyContext : DbContext
     {
          protected override void OnModelCreating(DbModelBuilder modelBuilder)
          {            
               modelBuilder.Entity<Student>()
                .HasMany(p => p.Hobbies)
                .WithMany(t => t.Students)
                .Map(mc =>
                {
                     mc.ToTable("StudentJoinHobby");
                     mc.MapLeftKey("StudentId");
                     mc.MapRightKey("HobbyId");
                });
          }
          public DbSet<student_and_hobby.Models.Group> Group { get; set; }

          public DbSet<student_and_hobby.Models.Student> Student { get; set; }

          public DbSet<student_and_hobby.Models.Hobby> Hobby { get; set; }
     }
}