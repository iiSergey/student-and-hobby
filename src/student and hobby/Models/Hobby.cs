﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace student_and_hobby.Models
{
     public class Hobby
     {
          public int HobbyId { get; set; }

          [Required(ErrorMessage = "Введіть назву хобі", AllowEmptyStrings = false)]
          [MaxLength(100, ErrorMessage = "Назва надто довга")]
          public string Name { get; set; }

          public virtual ICollection<Student> Students { get; set; }
     }
}